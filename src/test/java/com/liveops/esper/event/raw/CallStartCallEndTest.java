package com.liveops.esper.event.raw;

import com.liveops.esper.Classifiable;
import com.liveops.esper.View;
import com.liveops.esper.Views;
import com.liveops.esper.event.Classified;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertTrue;

/**
 * @author bruno
 */
public class CallStartCallEndTest {
    Collection<View> views;
    static final int CCID = 1;
    static final long NOW = System.currentTimeMillis();
    static final int ID = 1;
    static final int SESSION_ID = 1;
    static final int CAMPAIGN_ID = 1;

    @Before
    public void setUpViews() {
        views = Arrays.asList(Views.all("foo"), Views.all("bar"), Views.all("baz"));
    }

    @Test
    public void testClassify() throws Exception {
       testClassify(new CallStart(ID, NOW, CCID, SESSION_ID, CAMPAIGN_ID));
       testClassify(new CallEnd(ID, NOW, CCID, SESSION_ID, CAMPAIGN_ID));

    }


    private void testClassify(Classifiable unclassified) throws Exception {
        for (View view : views) {
            Classified classified = unclassified.assignView(view);
            assertTrue(classified.getView().equals(view));
        }
    }
}
