package com.liveops.esper.classifier;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.liveops.esper.View;
import com.liveops.esper.Views;
import com.liveops.esper.event.Classified;
import com.liveops.esper.event.Event;
import com.liveops.esper.event.raw.CallStart;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import java.util.*;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class ClassifierTest {
    List<View> testViews;
    Set<View> trueViews;
    Classifier c;

    @Before
    public void setUp() {
        testViews = Arrays.asList(Views.all(), Views.all("other"), Views.none());
        trueViews = Sets.newHashSet(testViews.get(0), testViews.get(1));
        c = new Classifier(testViews.toArray(new View[]{}));
    }

    @Test
    public void testClassify() throws Exception {
        Event e = mock(Event.class);

        assertEquals(c.classify(e), trueViews);
    }

    @Test
    public void testBroadcast() throws Exception {
        CallStart e = new CallStart(1, 1, 1, 1, 1);

        List<Classified> events = c.broadcast(e);
        assertTrue(events.size() == trueViews.size());

        Set<View> views = new HashSet(events.size());
        for (Classified ev : events) views.add(ev.getView());

        assertEquals(views, trueViews);
    }
}
