package com.liveops.esper.metric.listener;

import com.liveops.esper.MetricValue;
import com.liveops.esper.View;
import com.liveops.esper.Views;
import org.junit.Before;
import org.junit.Test;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class RedisUpdateListenerTest {
    Jedis jedisMock;
    JedisPool poolMock;
    RedisUpdateListener<Double> redisListener;

    @Before
    public void setUp() throws Exception {
        jedisMock = mock(Jedis.class);
        when(jedisMock.ping()).thenReturn("PONG");

        poolMock = mock(JedisPool.class);
        when(poolMock.getResource()).thenReturn(jedisMock);

        redisListener = new RedisUpdateListener<Double>(poolMock);
    }

    @Test
    public void testUpdate() throws Exception {
        View allView = Views.all();
        redisListener.update(new MetricValue<Double>("testMetric", 1.0, Views.all()));

        verify(jedisMock).set(allView.getName() + ".testMetric", "1.0");
    }
}
