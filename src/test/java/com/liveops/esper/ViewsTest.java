package com.liveops.esper;

import com.liveops.esper.event.Event;
import com.liveops.esper.event.SessionEvent;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ViewsTest {
    View campaign10, campaign42, ccid80;
    SessionEvent campaign10Event, campaign42Event, campaign10Ccid80Event;
    Event plainEvent, ccid80Event;

    @Before
    public void setUp() {
        campaign10 = Views.byCampaignId("campaign10", 10);
        campaign42 = Views.byCampaignId("campaign42", 42);

        campaign10Event = mock(SessionEvent.class);
        when(campaign10Event.getCampaignId()).thenReturn(10);

        campaign10Ccid80Event = mock(SessionEvent.class);
        when(campaign10Ccid80Event.getCampaignId()).thenReturn(10);
        when(campaign10Ccid80Event.getCallcenterId()).thenReturn(80);

        campaign42Event = mock(SessionEvent.class);
        when(campaign42Event.getCampaignId()).thenReturn(42);

        plainEvent = mock(Event.class);

        ccid80 = Views.byCallcenterId("callcenter80", 80);
        ccid80Event = mock(Event.class);
        when(ccid80Event.getCallcenterId()).thenReturn(80);
    }

    @Test
    public void testByCampaign() throws Exception {
        assertTrue(campaign10.matches(campaign10Event));
        assertFalse(campaign42.matches(campaign10Event));

        assertFalse(campaign10.matches(campaign42Event));
        assertTrue(campaign42.matches(campaign42Event));

        assertFalse(campaign10.matches(plainEvent));
        assertFalse(campaign42.matches(plainEvent));
    }

    @Test
    public void testByCallcenterId() throws Exception {
        assertTrue(ccid80.matches(ccid80Event));
        assertFalse(ccid80.matches(plainEvent));
        assertFalse(ccid80.matches(campaign42Event));
    }

    @Test
    public void testCombinations() throws Exception {
        View campaign10or42 = campaign10.or(campaign42);

        assertTrue(campaign10or42.matches(campaign10Event));
        assertTrue(campaign10or42.matches(campaign42Event));

        assertFalse(campaign10or42.matches(plainEvent));

        assertEquals("campaign10 campaign42 |", campaign10or42.getName());

        View campaign10Ccid80 = campaign10.and(ccid80);
        assertFalse(campaign10Ccid80.matches(campaign10Event));
        assertFalse(campaign10Ccid80.matches(ccid80Event));
        assertTrue(campaign10Ccid80.matches(campaign10Ccid80Event));

        assertEquals("campaign10 callcenter80 &", campaign10Ccid80.getName());

        View allButCampaign42 = campaign42.not();

        assertFalse(allButCampaign42.matches(campaign42Event));
        assertTrue(allButCampaign42.matches(plainEvent));
        assertTrue(allButCampaign42.matches(campaign10Event));

        assertEquals("!(campaign42)", allButCampaign42.getName());

    }

}
