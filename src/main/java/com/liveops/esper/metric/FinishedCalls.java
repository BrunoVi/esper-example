package com.liveops.esper.metric;

public class FinishedCalls extends BaseTimedMetric {
    private final String epl;

    private static final String EPL_FORMAT =
            "select count(*) as %s, view " +
            "from FinishedCall%s group by view";

    public FinishedCalls(int timeWindow) {
        super(timeWindow);
        epl = String.format(EPL_FORMAT, getName(), buildTimeWindowEPL(timeWindow));
    }

    public String getEPL() {
        return epl;
    }
}
