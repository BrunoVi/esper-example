package com.liveops.esper.metric.listener;

import com.liveops.esper.MetricValue;

public interface MetricUpdateListener<T> {
    public void update(MetricValue<T> newValue);
}
