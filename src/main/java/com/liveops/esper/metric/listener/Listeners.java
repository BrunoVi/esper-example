package com.liveops.esper.metric.listener;

import com.espertech.esper.client.EventBean;
import com.espertech.esper.client.UpdateListener;
import com.liveops.esper.MetricValue;
import com.liveops.esper.View;

/**
 * Static factory of UpdateListeners from the easier MetricUpdateListeners
 *
 * @author bruno
 */
public class Listeners {

    // TODO: proper formatting of this

    /**
     * Given one or more MetricUpdateListener, it gives you an UpdateListener that:
     * * takes the first event
     * * extracts a View in a 'view' field
     * * extracts a value of type T from a the {@code metricName} field
     * * produces a {@link MetricValue<T>} instance with name {@code metricName}, the extracted view and value.
     * * calls each {@code MetricUpdateListener}'s {@code onUpdate(MetricValue value)}
     *
     * @param listeners
     * @param metricName
     * @param <T>
     * @return
     */
    public static <T> UpdateListener

    fromMetric(
        final String metricName,
        final MetricUpdateListener<T>... listeners)
    {
        return new UpdateListener() {
            public void update(EventBean[] newEvents, EventBean[] oldEvents) {
                View view = (View) newEvents[0].get("view");
                T value = (T) newEvents[0].get(metricName);
                MetricValue<T> newValue = new MetricValue<T>(metricName, value, view);

                for (MetricUpdateListener<T> listener : listeners)
                    listener.update(newValue);
            }
        };
    }
}
