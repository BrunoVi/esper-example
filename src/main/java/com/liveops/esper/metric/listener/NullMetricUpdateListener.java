package com.liveops.esper.metric.listener;

import com.liveops.esper.MetricValue;

/**
 * @author bruno
 */
public class NullMetricUpdateListener<T> implements MetricUpdateListener<T> {

    public void update(MetricValue<T> newValue) { }
}
