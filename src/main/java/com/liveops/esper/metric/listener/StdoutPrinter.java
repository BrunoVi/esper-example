package com.liveops.esper.metric.listener;


import com.liveops.esper.MetricValue;

public class StdoutPrinter<T> implements MetricUpdateListener<T> {

    public void update(MetricValue<T> newValue) {
        System.out.println(newValue);
    }

}
