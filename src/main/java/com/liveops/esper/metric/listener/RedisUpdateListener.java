package com.liveops.esper.metric.listener;

import com.liveops.esper.MetricValue;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static com.google.common.base.Preconditions.checkNotNull;

public class RedisUpdateListener<T> implements MetricUpdateListener<T> {
    private final JedisPool pool;

    public RedisUpdateListener(JedisPool pool) {
        this.pool = checkNotNull(pool);

//        assert "PONG".equals(client.ping()) : "Redis works.";
    }

    @Override
    public void update(final MetricValue<T> value) {

        Jedis client = pool.getResource();
        try {
            client.set(getKey(value), value.getValue().toString());
        } finally {
            pool.returnResource(client);
        }
    }

    private String getKey(MetricValue<T> value) {
        return value.getView() + "." + value.getMetricName();
    }
}
