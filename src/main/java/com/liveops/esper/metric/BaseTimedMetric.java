package com.liveops.esper.metric;

import static com.google.common.base.Preconditions.checkArgument;

/**
 * @author bruno
 */
public abstract class BaseTimedMetric implements MetricDefinition {
    // timeWindow:  For now we only express them in seconds, might have to use a duration object here.
    private final String metricName;

    BaseTimedMetric(int timeWindow) {
        checkArgument(timeWindow >= 0);
        metricName = buildMetricName(timeWindow);
    }

    public String getName() {
       return metricName;
    }

    public abstract String getEPL();

    protected String buildMetricName(int timeWindow) {
        return this.getClass().getSimpleName() + (timeWindow > 0 ? "_" + timeWindow + "s" : "");
    }

    protected static String buildTimeWindowEPL(int timeWindow) {
        return (timeWindow > 0 ? String.format(".win:time(%d sec)", timeWindow) : "");
    }
}
