package com.liveops.esper.metric;

/**
 * @author bruno
 */
public interface MetricDefinition {
    public String getEPL();
    public String getName();
}
