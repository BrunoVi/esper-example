package com.liveops.esper.metric;

public class AvgSessionLength extends BaseTimedMetric {
    private final String epl;

    private static final String EPL_TEMPLATE =
            "SELECT avg(duration) AS %s, view " +
            "FROM FinishedCall%s group by view";

    public AvgSessionLength(int timeWindow) {
        super(timeWindow);
        this.epl = String.format(EPL_TEMPLATE, getName(), buildTimeWindowEPL(timeWindow));
    }

    public String getEPL() { return epl; }
}