package com.liveops.esper;

public interface Agent {
    public int getAgentId();
}