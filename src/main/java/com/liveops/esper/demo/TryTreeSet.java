package com.liveops.esper.demo;

import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author bruno
 */
public class TryTreeSet {
    public static void main(String[] args) {
        TreeSet<String> byLength = new TreeSet<String>(new Comparator<String>() {
            public int compare(String s1, String s2) {
                if (s1.length() > s2.length()) return +1;
                else return -1;
            }
        });

        byLength.add("foo");
        byLength.add("bar");
        byLength.add("baz");
        byLength.add("longer");
        byLength.add("a");

        for (String s : byLength.headSet("aaaa")) {
            System.out.println(s);
        }

        assert byLength.contains("bar");
        assert byLength.contains("foo");
        assert byLength.contains("baz");
    }
}
