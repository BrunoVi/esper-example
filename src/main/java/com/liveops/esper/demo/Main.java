package com.liveops.esper.demo;

import java.util.Arrays;
import java.util.Collection;
import java.util.Random;

import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import com.espertech.esper.client.Configuration;
import com.espertech.esper.client.EPRuntime;
import com.espertech.esper.client.EPServiceProvider;
import com.espertech.esper.client.EPServiceProviderManager;
import com.liveops.esper.RTM;
import com.liveops.esper.RTMBuilder;
import com.liveops.esper.View;
import com.liveops.esper.Views;
import com.liveops.esper.event.complex.FinishedCall;
import com.liveops.esper.event.raw.CallEnd;
import com.liveops.esper.event.raw.CallStart;
import com.liveops.esper.metric.AvgSessionLength;
import com.liveops.esper.metric.FinishedCalls;
import com.liveops.esper.metric.listener.MetricUpdateListener;
import com.liveops.esper.metric.listener.RedisUpdateListener;
import com.liveops.esper.metric.listener.StdoutPrinter;

public class Main {

    public static void main(String[] args) throws InterruptedException {

        // Set multiple views for each metric
        Collection<View> views = Arrays.asList(
            Views.all("all"),
            Views.byCallcenterId(1),
            Views.byCallcenterId(1).and(Views.byCampaignId(2).or(Views.byCampaignId(3))),
            Views.byCallcenterId(2),
            Views.byCallcenterId(2).and(Views.byCampaignId(5))
        );

        // Redis setup
        JedisPool pool = new JedisPool(new JedisPoolConfig(), "localhost");
        MetricUpdateListener<Object> redisListener = new RedisUpdateListener<Object>(pool);
        MetricUpdateListener<Object> stdoutPrinter = new StdoutPrinter<Object>();

        // Perform demo
        RTM demo = new RTMBuilder()
                .setViews(views)
                // uncomment this to activate stdout logging of metric values
                .setListeners(redisListener /*, stdoutPrinter */)
                .build();

        demo
            .registerComplexEvent(FinishedCall.EPL)
            .registerMetric(new FinishedCalls(0))
            .registerMetric(new FinishedCalls(30))
            .registerMetric(new AvgSessionLength(0));

        long now = System.currentTimeMillis();
        int campaignId, ccid, duration;
        int sessionId = 0;
        Random rnd = new Random();

        while (true) {
            ccid = 1 + rnd.nextInt(4);
            campaignId = 1 + rnd.nextInt(11);
            duration = 1 + rnd.nextInt(11);
            ++sessionId;

            demo.sendEvent(new CallStart(nextId(), now, ccid, sessionId, campaignId));
            demo.sendEvent(new CallEnd(nextId(), now + duration, ccid, sessionId, campaignId));
            Thread.sleep(100);
        }
    }

    private static int nextId = 1;

    private static int nextId() {
        return nextId++;
    }

}
