package com.liveops.esper.demo;

import com.liveops.esper.Session;
import com.liveops.esper.Views;
import com.liveops.esper.event.Classified;
import com.liveops.esper.event.Event;
import com.liveops.esper.event.SessionEvent;
import com.liveops.esper.event.Timed;
import com.liveops.esper.event.complex.ComplexEvent;
import com.liveops.esper.event.complex.FinishedCall;

/**
 * Created with IntelliJ IDEA.
 * User: bruno
 * Date: 1/10/13
 * Time: 4:17 PM
 * To change this template use File | Settings | File Templates.
 */
public class TestBed {
    public static void main(String[] args) {
        long now = System.currentTimeMillis();
        FinishedCall event = new FinishedCall(now, 80, 1, 1, now - 1000, now, Views.all());

        System.out.println(event);

        assert event instanceof FinishedCall;
        assert event instanceof ComplexEvent;
        assert event instanceof Event;
        assert event instanceof Session;
        assert event instanceof SessionEvent;
        assert event instanceof Timed;
        assert event instanceof Classified;

        various();
        System.out.println("out of various");
    }

    public static void various(String... stuff) {
        System.out.println("in various");
        for (String thing : stuff) System.out.println(thing);
    }
}
