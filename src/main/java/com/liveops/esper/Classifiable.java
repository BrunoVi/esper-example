package com.liveops.esper;

import com.liveops.esper.event.Classified;
import com.liveops.esper.event.Event;

import javax.annotation.Nonnull;

/**
 * Classifiable events implement a {@code assignView} method that returns a clone of themselves with a {@link View}.
 * <p/>
 * Implementations of this interface should allow for cheap creation of multiple clones of the same underlying event
 * with many views without using O(n) memory.
 */
public interface Classifiable extends Event {

    /**
     * Get a classified event
     * @param view
     * @return a clone of the invocant with a non-null View.
     */
    Classified assignView(View view);
}
