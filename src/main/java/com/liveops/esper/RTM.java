package com.liveops.esper;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Collection;

import com.espertech.esper.client.Configuration;
import com.espertech.esper.client.EPServiceProvider;
import com.espertech.esper.client.EPServiceProviderManager;
import com.liveops.esper.classifier.Classifier;
import com.liveops.esper.event.Classified;
import com.liveops.esper.metric.MetricDefinition;
import com.liveops.esper.metric.listener.Listeners;
import com.liveops.esper.metric.listener.MetricUpdateListener;

/**
 * @author bvecchi
 */
public class RTM { // TODO: name??
    private final EPServiceProvider esper;
    private final MetricUpdateListener<?>[] listeners;
    private final Classifier classifier;

    /**
     * Constructor.
     * @param config
     * @param views
     * @param listeners
     */
    RTM(Configuration config, Collection<View> views, MetricUpdateListener<?>... listeners) {
        this.listeners = listeners;
        this.classifier = new Classifier(views);
        this.esper = EPServiceProviderManager.getDefaultProvider(config);
    }

    /**
     * Responsible for registering a complex event into ESPER
     * @param eventDefinition
     * @return
     */
    public RTM registerComplexEvent(String eventDefinition) {
        esper.getEPAdministrator().createEPL(checkNotNull(eventDefinition));
        return this;
    }

    /**
     * Responsible for registering a metric into ESPER
     * @param metric
     * @return
     */
    public RTM registerMetric(MetricDefinition metric) {
        checkNotNull(metric);
        checkNotNull(listeners);
        esper.getEPAdministrator()
                .createEPL(metric.getEPL())
                .addListener(Listeners.fromMetric(
                        metric.getName(), (MetricUpdateListener<Object>[])listeners)
                );

        return this;
    }

    /**
     * Responsible for broadcasting an event.
     * @param event
     * @return
     */
    public RTM sendEvent(Classifiable event) {
        for (Classified e : classifier.broadcast(event)) {
            System.out.println(e);
            esper.getEPRuntime().sendEvent(e);
        }

        return this;
    }
}
