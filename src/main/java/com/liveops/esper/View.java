package com.liveops.esper;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.ArrayList;
import java.util.List;

import com.google.common.base.Objects;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.liveops.esper.event.Event;


/**
 * @author bruno
 */
public class View {
    private final String name;
    private final Predicate<Event> matcher;

    /**
     * Constructor
     * @param name
     * @param matcher
     */
    View(String name, Predicate<Event> matcher) {
        this.name = checkNotNull(name);
        this.matcher = checkNotNull(matcher);
    }

    /**
     * Add more views to the Predicate matchers list and return also
     * the name representing the concatenation of all the views in reverse
     * polish notation with the AND operators.
     * @param other
     * @return
     */
    public View and(View... other) {
        String name = newViewName("&", this, other);
        List<Predicate<Event>> matchers = new ArrayList<Predicate<Event>>(other.length + 1);
        matchers.add(getMatcher());

        for (View v : other) {
            matchers.add(v.getMatcher());
        }

        return new View(name, Predicates.<Event>and(matchers));
    }

    /**
     * Add more views to the Predicate matchers list and return also
     * the name representing the concatenation of all the views in reverse
     * polish notation with the OR operators.
     * @param other
     * @return
     */
    public View or(View... other) {
        String name = newViewName("|", this, other);
        List<Predicate<Event>> matchers = new ArrayList<Predicate<Event>>(other.length + 1);
        matchers.add(getMatcher());

        for (View v : other) {
            matchers.add(v.getMatcher());
        }

        return new View(name, Predicates.<Event>or(matchers));
    }

    /**
     * Unify all the views with the operator as a hole String in
     * reverse polish notation.
     * @param op
     * @param view
     * @param views
     * @return
     */
    private static String newViewName(String op, View view, View... views) {
        if (views.length == 0) return view.getName();

        StringBuilder sb = new StringBuilder(view.getName());

        for (View v : views) {
            sb.append(" ").append(v.getName());
        }

        for (int i = 0; i < views.length; i++) {
            sb.append(" ").append(op);
        }

        return sb.toString();
    }

    public View not() {
        return new View("!(" + getName() + ")", Predicates.<Event>not(this.getMatcher()));
    }

    public boolean matches(Event e) {
        return matcher.apply(e);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof View) {
            View other = (View)obj;
            return getName().equals(other.getName()) && getMatcher().equals(other.getMatcher());
        }
        return false;
    }

    @Override
    public String toString() {
        return getName();
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getName(), getMatcher());
    }
    
    public String getName() {
        return name;
    }

    public Predicate<Event> getMatcher() {
        return matcher;
    }
}
