package com.liveops.esper.classifier;

import com.google.common.collect.ImmutableSet;
import com.liveops.esper.Classifiable;
import com.liveops.esper.View;
import com.liveops.esper.event.Classified;
import com.liveops.esper.event.Event;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * Given an event, assignView it into zero or more {@link View}s.
 * The list of Views known to a Classifier instance can be only set at construction time.
 *
 * This class is immutable and thread safe.
 */
public class Classifier {
    private final ImmutableSet<View> views;

    public Classifier(View... views) {
        this.views = ImmutableSet.copyOf(views);
    }

    public Classifier(Collection<View> views) {
        this.views = ImmutableSet.copyOf(views);
    }

    public List<Classified> broadcast(Classifiable e) {
        List<Classified> events = new ArrayList<Classified>();
        for (View view : classify(e)) {
            events.add(e.assignView(view));
        }

        return events;
    }

    /**
     * Given an {@link Event}, return the set of {@link View}s that it belongs to.
     * @param e an object that implements the {@link Event} interface
     * @return a possibly empty, immutable set of {@link View}s
     */
    public Set<View> classify(Event e) {
        ImmutableSet.Builder set = ImmutableSet.builder();
        for (View v : views) {
            if (v.matches(e)) set.add(v);
        }

        return set.build();
    }
}