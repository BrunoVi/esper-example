package com.liveops.esper;

public interface Session {
    public int getSessionId();
    public int getCampaignId();
}
