package com.liveops.esper.event.raw;

import com.liveops.esper.Classifiable;
import com.liveops.esper.event.Event;

/**
 * A low-level {@link Event}, directly received from Raven.
 * Its defining attribute is an event id.
 */
public interface RawEvent {
    /**
     * @return the event id
     */
    public int getId();
}
