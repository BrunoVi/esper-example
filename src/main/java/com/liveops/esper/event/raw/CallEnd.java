package com.liveops.esper.event.raw;

import com.liveops.esper.Classifiable;
import com.liveops.esper.View;
import com.liveops.esper.event.raw.internal.RawClassifiedSessionEvent;
import com.liveops.esper.event.raw.internal.RawSessionEvent;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * @author bruno
 */
public class CallEnd extends RawClassifiedSessionEvent implements Classifiable {

    public CallEnd(int id, long timestamp, int callcenterId, int sessionId, int campaignId ) {
        this(id, timestamp, callcenterId, sessionId, campaignId, null);
    }

    public CallEnd(int id, long timestamp, int callcenterId, int sessionId, int campaignId, @Nullable View view) {
        super(id, timestamp, callcenterId, sessionId, campaignId, view);
    }

    protected CallEnd(RawSessionEvent event, View view) {
        super(event, view);
    }

    @Override
    public CallEnd assignView(View view) {
        return new CallEnd(this.event, view);
    }
}