package com.liveops.esper.event.raw.internal;

import com.google.common.base.Objects;
import com.liveops.esper.Session;
import com.liveops.esper.View;
import com.liveops.esper.event.Classified;
import com.liveops.esper.event.SessionEvent;
import com.liveops.esper.event.raw.RawEvent;

import javax.annotation.Nullable;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author bruno
 */
public class RawClassifiedSessionEvent implements RawEvent, Classified, Session {
    protected final RawSessionEvent event;
    private final @Nullable View view;

    public RawClassifiedSessionEvent(int id, long timestamp, int callcenterId, int sessionId, int campaignId, @Nullable View view) {
        this.event = new RawSessionEvent(id, timestamp, callcenterId, sessionId, campaignId);
        // When calling from this constructor, view is possibly null since we could be behind the classifier
        this.view = view;
    }

    protected RawClassifiedSessionEvent(RawSessionEvent event, View view) {
        this.event = event;
        // Here we won't allow null. The purpose of this protected constructor is to be used by assignView(View v) calls
        // that create clones with an attached view.
        this.view = checkNotNull(view);
    }

    public int getId() {
        return event.getId();
    }

    public long getTimestamp() {
        return event.getTimestamp();
    }

    public int getCallcenterId() {
        return event.getCallcenterId();
    }

    public int getSessionId() {
        return event.getSessionId();
    }

    public int getCampaignId() {
        return event.getCampaignId();
    }

    public @Nullable View getView() {
        return view;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this.getClass().getSimpleName())
                .add("id", getId())
                .add("timestamp", getTimestamp())
                .add("callcenterId", getCallcenterId())
                .add("sessionId", getSessionId())
                .add("campaignId", getCampaignId())
                .add("view", getView())
                .toString();
    }
}