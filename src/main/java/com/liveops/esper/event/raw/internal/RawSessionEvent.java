package com.liveops.esper.event.raw.internal;

import com.google.common.base.Objects;
import com.liveops.esper.Session;
import com.liveops.esper.View;
import com.liveops.esper.event.Classified;
import com.liveops.esper.event.SessionEvent;
import com.liveops.esper.event.raw.RawEvent;

import javax.annotation.Nullable;

/**
 * @author bruno
 */
public class RawSessionEvent extends SessionEvent implements RawEvent, Session {
    public final int id;

    RawSessionEvent(int id, long timestamp, int callcenterId, int sessionId, int campaignId) {
        super(timestamp, callcenterId, sessionId, campaignId);
        this.id = id;
    }

    public int getId() {
        return id;
    }

}