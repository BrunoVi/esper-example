package com.liveops.esper.event.raw;

import com.liveops.esper.Classifiable;
import com.liveops.esper.View;
import com.liveops.esper.event.raw.internal.RawClassifiedSessionEvent;
import com.liveops.esper.event.raw.internal.RawSessionEvent;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * A low-level event that signifies the creation of a user session.
 * @author bruno
 */
public class CallStart extends RawClassifiedSessionEvent implements Classifiable {

    public CallStart(int id, long timestamp, int callcenterId, int sessionId, int campaignId) {
        this(id, timestamp, callcenterId, sessionId, campaignId, null);
    }

    public CallStart(int id, long timestamp, int callcenterId, int sessionId, int campaignId, @Nullable View view) {
        super(id, timestamp, callcenterId, sessionId, campaignId, view);
    }

    protected CallStart(RawSessionEvent event, View view) {
        super(event, view);
    }

    @Override
    public CallStart assignView(View view) {
        return new CallStart(this.event, view);
    }
}
