package com.liveops.esper.event;

import com.google.common.base.Objects;
import com.liveops.esper.Session;

import static com.google.common.base.Preconditions.*;

/**
 * A base class for session events.
 */
public abstract class SessionEvent implements Session, Event {
    private final long timestamp;
    private final int callcenterId, sessionId, campaignId;

    public SessionEvent(long timestamp, int callcenterId, int sessionId, int campaignId) {

        checkArgument(callcenterId > 0, "callcenterId should be > 0, was %s", callcenterId);
        checkArgument(timestamp > 0, "Timestamp should be > 0, was %s", timestamp);
        checkArgument(sessionId > 0, "sessionId should be > 0, was %s", sessionId);
        checkArgument(campaignId > 0, "campaignId should be > 0, was %s", sessionId);

        this.callcenterId = callcenterId;
        this.timestamp = timestamp;
        this.sessionId = sessionId;
        this.campaignId = campaignId;
    }

    public int getCallcenterId() {
        return callcenterId;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public int getSessionId() {
        return sessionId;
    }

    public int getCampaignId() {
        return campaignId;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("timestamp", getTimestamp())
                .add("callcenterId", getCallcenterId())
                .add("sessionId", getSessionId())
                .add("campaignId", getCampaignId())
                .toString();
    }
}
