package com.liveops.esper.event;

/**
 * A basic interface for an event. Anything can be an event as long as it has a callcenter id and a timestamp.
 * @author bruno
 */
public interface Event extends Timed {
    /**
     * @return the callcenter id of the event.
     */
    public int getCallcenterId();
}
