package com.liveops.esper.event.complex;

import com.google.common.base.Objects;
import com.liveops.esper.View;
import com.liveops.esper.event.SessionEvent;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

public class FinishedCall extends SessionEvent implements ComplexEvent {
    private final long startTime;
    private final long endTime;
    private final long duration;
    private final View view;

    public static final String EPL =
            "insert into FinishedCall(timestamp, callcenterId, sessionId, campaignId, startTime, endTime, view) " +
            "select e.timestamp, e.callcenterId, s.sessionId, s.campaignId, s.timestamp, e.timestamp as endTime, s.view " +
            "from pattern [every s=CallStart -> e=CallEnd(sessionId = s.sessionId and view=s.view) where timer:within(12 hours)]";

    public FinishedCall(long timestamp, int callcenterId, int sessionId, int campaignId, long startTime, long endTime, View view) {
        super(timestamp, callcenterId, sessionId, campaignId);

        checkArgument(startTime > 0, "StartTime should be > 0, was %s.", startTime);
        checkArgument(endTime > 0, "EndTime should be > 0, was %s.", endTime);
        checkArgument(startTime < endTime, "StartTime (%s) should be < EndTime (%s).", startTime, endTime);

        this.startTime = startTime;
        this.endTime = endTime;
        this.duration = endTime - startTime;
        assert duration > 0;

        // At the point where high-level events are created, the events are classified.
        this.view = checkNotNull(view);
        // This better be true!
        assert view.matches(this);

        // TODO: replace with logging
//        System.out.println(this);
    }

    public long getEndTime() {
        return endTime;
    }

    public long getStartTime() {
        return startTime;
    }

    public long getDuration() {
        return duration;
    }

    public View getView() {
        return view;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("timestamp",getTimestamp())
                .add("callcenterId", getCallcenterId())
                .add("sessionId", getSessionId())
                .add("campaignId", getCampaignId())
                .add("startTime", getStartTime())
                .add("endTime", getEndTime())
                .add("duration", getDuration())
                .add("view", getView())
                .toString();
    }
}
