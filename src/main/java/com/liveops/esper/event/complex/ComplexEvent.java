package com.liveops.esper.event.complex;

import com.liveops.esper.event.Classified;
import com.liveops.esper.event.Event;

/**
 * A high-level {@link Event}. High level events are emitted by the CEP engine upon analysis of the raw event stream. A
 * simple example would be two events A, B triggering the generation of a third, high level event C.
 * <p/>
 * Additionally, high-level events can also trigger high-level events.
 * <p/>
 * High level events do not have raw event ids. Since they are generated downstream of the {@link
 * com.liveops.esper.classifier.Classifier}, they are guaranteed to have a {@link com.liveops.esper.View}.
 */
public interface ComplexEvent extends Event, Classified {

}
