package com.liveops.esper.event;

import com.liveops.esper.View;


/**
 * An {@link Event} that has been classified as part of a {@link View}
 * @author bruno
 */
public interface Classified extends Event {
    /**
     * @return The {@link View} that this Event is part of.
     */
    public View getView();
}
