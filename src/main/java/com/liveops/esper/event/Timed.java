package com.liveops.esper.event;

public interface Timed {
    public long getTimestamp();
}