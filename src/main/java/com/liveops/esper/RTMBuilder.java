package com.liveops.esper;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Collection;

import com.espertech.esper.client.Configuration;
import com.liveops.esper.event.complex.FinishedCall;
import com.liveops.esper.event.raw.CallEnd;
import com.liveops.esper.event.raw.CallStart;
import com.liveops.esper.metric.listener.MetricUpdateListener;

/**
 * Builder for the RTM demo application. 
 * Has the necessary methods to setup properly the ESPER demo.
 * @author bvecchi
 *
 */
public class RTMBuilder {
    private Configuration config = defaultConfig();
    private Collection<View> views;
    private MetricUpdateListener<?>[] listeners;

    /**
     * Config setup method.
     * @param config
     * @return
     */
    public RTMBuilder setConfig(Configuration config) {
        this.config = checkNotNull(config);
        return this;
    }

    /**
     * Views setup method.
     * @param views
     * @return
     */
    public RTMBuilder setViews(Collection<View> views) {
        this.views = checkNotNull(views);
        return this;
    }

    /**
     * Listeners setup method.
     * @param listeners
     * @return
     */
    public RTMBuilder setListeners(MetricUpdateListener<?>... listeners) {
        this.listeners = checkNotNull(listeners);
        return this;
    }

    /**
     * Build method responsible for instantiate the RTM class.
     * @return
     */
    public RTM build() {
        return new RTM(config, views, listeners);
    }

    /**
     * Default config setup method.
     * @return
     */
    private static Configuration defaultConfig() {
        Configuration c = new Configuration();
        c.addEventType(CallStart.class);
        c.addEventType(CallEnd.class);
        c.addEventType(FinishedCall.class);

        return c;
    }
}