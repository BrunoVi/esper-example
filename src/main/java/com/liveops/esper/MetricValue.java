package com.liveops.esper;

import javax.annotation.Nonnull;

import com.google.common.base.Objects;

public class MetricValue<T> {
    private final T value;
    private final View view;
    private final String metricName;

    public MetricValue(String metricName, T value, View view) {
        this.metricName = metricName;
        this.value = value;
        this.view = view;
    }

    public String getMetricName() {
        return metricName;
    }

    public T getValue() {
        return value;
    }

    public View getView() {
        return view;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(metricName, view, value);
    }

    // TODO: implement equals

    @Override
    public String toString() {
        return Objects.toStringHelper(this).addValue(metricName).addValue(value).addValue(view).toString();
    }

}
