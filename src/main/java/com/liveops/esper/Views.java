package com.liveops.esper;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.liveops.esper.event.Event;

public class Views {

    private Views() {}

    /**
     * Static method to perform metrics by campaign id.
     * @param name
     * @param campaignId
     * @return
     */
    public static View byCampaignId(String name, final int campaignId) {

        Predicate<Event> byCampaign = new Predicate<Event>() {
            @Override
            public boolean apply(Event e) {
                if (e == null || !(e instanceof Session)) return false;
                return ((Session)e).getCampaignId() == campaignId;
            }
        };

        return new View(name, byCampaign);
    }

    /**
     * Static overloaded method to perform metrics by campaign id
     * @param campaignId
     * @return
     */
    public static View byCampaignId(final int campaignId) {
        String name = "campaign_id=" + campaignId;
        return byCampaignId(name, campaignId);
    }

    /**
     * Static method to perform metrics by callcenter id
     * @param name
     * @param callcenterId
     * @return
     */
    public static View byCallcenterId(String name, final int callcenterId) {
        Predicate<Event> byCcId = new Predicate<Event>() {

            @Override
            public boolean apply(Event e) {
                if (e == null) return false;

                return e.getCallcenterId() == callcenterId;
            }
        };

        return new View(name, byCcId);
    }

    /**
     * Static overloaded method to perform metrics by callcenter id
     * @param callcenterId
     * @return
     */
    public static View byCallcenterId(final int callcenterId) {
        return byCallcenterId("callcenterId=" + callcenterId, callcenterId);
    }

    public static View none() {
        return Views.none("none");
    }

    public static View none(String name) {
        return new View(name, Predicates.<Event>alwaysFalse());
    }

    public static View all() {
        return Views.all("foo");
    }

    public static View all(String name) {
        return new View(name, Predicates.<Event>alwaysTrue());
    }

}
